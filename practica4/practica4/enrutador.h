#ifndef ENRUTADOR_H
#define ENRUTADOR_H
#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <string.h>

using namespace std;

//int** tablaEnrutamiento;

class enrutador{
    private:
    map<string,int> conexiones;
    string nombre;
    public:
    enrutador(string name);
    void conectar(string A, int B);
    void desconectar(string A);
    void test();
    void costoConexionCon(string a);
    int costoConexion(string a);
    void mostrarConexiones();
    map<string,int>* getConexiones();
    string getEnrutadorNombre();
    friend bool operator== ( const enrutador &en1, const enrutador &en2);


};

class Red{
    private:
    vector<enrutador> enrutadores;
    string nombre;
    public:
    Red(string nombre);
    bool validarRed();
    void setEnrutador(enrutador a);
    enrutador getEnrutador(string name);
    void deleteEnrutador(string a);
    void updateEnrutador(string a);
    void actualizarEnrutador(string routOrig, string RoutDest, int cost);
    void showListEnrutadores();
    void showConexionesEnrutadores();
    void showConexionesEnrutador(string a);
    bool existeEnrutador(enrutador temp);
    bool existeEnrutador(string a);
    void calcularCostoSrcDst(string src,string dst);
    void calcularCostoMejorCamino(string src,string dst);
    void calcularTabla();
    void mostrarTablaEnrutamiento();

};


#endif // ENRUTADOR_H
