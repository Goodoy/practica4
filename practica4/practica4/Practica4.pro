TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
    funciones.cpp \
    enrutador.cpp

HEADERS += \
    funciones.h \
    enrutador.h
